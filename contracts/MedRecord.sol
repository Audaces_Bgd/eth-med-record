pragma solidity >=0.4.21 <0.7.0;
pragma experimental ABIEncoderV2;

contract MedRecord {
    Record[] public records;
    mapping (address => Account) accountData;

    struct Record {
        string title;
        string body;
        address author;
    }

    struct Account {
        string title;
        string accountType;
    }

    constructor() public {
        addAllAccountsToAccountData();
    }

    function addRecord(string memory title, string memory body) public {
        Record memory record;
        record.title = title;
        record.body = body;
        record.author = msg.sender;
        records.push(record);
    }

    function getRecords() public view returns (Record[] memory) {
        return records;
    }

    function addAccountToAccountData(address accountAddress, string memory title, string memory accountType) private {
        Account memory account;
        account.title = title;
        account.accountType = accountType;
        accountData[accountAddress] = account;
    }

    function addAllAccountsToAccountData() private {
        addAccountToAccountData(0xEA401eD461B425EFc698d2F0fcE2CE1A535a4243, "Doctor 1", "doctor");
        addAccountToAccountData(0x4Cd31fb82802b279ca4b84422342Af4f3E531052, "Doctor 2", "doctor");
        addAccountToAccountData(0xA429AF9ec6690984B9f8eB4a3eF042eb78b211fb, "Doctor 3", "doctor");
        addAccountToAccountData(0x4104eCF9b01f1Bc1f93D1B6fE745397c6FB7bd5D, "Doctor 4", "doctor");
        addAccountToAccountData(0xB28ABe56C54fDaF04aC0a8c7790433E3aa1110e6, "Doctor 5", "doctor");
        addAccountToAccountData(0x28194fEE3B1F3fB0Ef5313211A2946FC2ebb3085, "Insurance 1", "insurance");
        addAccountToAccountData(0x4D91103C7954F7dAfDD856521c4fBABDa578cCb9, "Insurance 2", "insurance");
        addAccountToAccountData(0x17198E1C1B9919D5171e73021322D24403725786, "Insurance 3", "insurance");
        addAccountToAccountData(0x28Bd730B0aB3c8D7b82d35e11116FB40a7dBe5fD, "Insurance 4", "insurance");
        addAccountToAccountData(0xa3c4a329CA5792023F2A3fAc474B12d5EAb7da6E, "Insurance 5", "insurance");
    }

    function getAccountData(address accountAddress) public returns (Account memory) {
        return accountData[accountAddress];
    }
}